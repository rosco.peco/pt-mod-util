#include <stdio.h>
#include <stdlib.h>
#include "pt_mod.h"

static uint8_t buffer[1048576];

int main(void) {

    FILE *f = fopen("../xenon2.mod", "rb");
    fseek(f, 0, SEEK_END);
    const long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);

    fread(buffer, fsize, 1, f);
    fclose(f);

    PtMod *mod = (PtMod*)buffer;

    printf("Loaded: %20s\n", mod->song_name);

    const int number = 20;

    PtMemorySample samples[31];
    PtPopulateMemorySamples(mod, samples);

    printf("Samples populated in memory; Will play #%d\n", number);
    printf("Sample length is %d words\n", samples[number].length);

    printf("First 5 words: 0x%04x 0x%04x 0x%04x 0x%04x 0x%04x\n", 
            BE2(samples[number].data[0]),
            BE2(samples[number].data[1]), 
            BE2(samples[number].data[2]), 
            BE2(samples[number].data[3]), 
            BE2(samples[number].data[4])
    );

    FILE *out = fopen("sample.raw", "wb");
    fwrite(samples[number].data, 2, samples[number].length, out);
    fclose(out);
}



